from math import *

#Points
A = [100,100]
B = [400,400]
C = [400,100]
D = [100,400]
S = [-10,-10] # Point Clicked by the user. Before user click and after user release the mouse, the value
              # of this point is x = -10 and y = -10
windowHeight = 500
windowWidth = 500

def setup():
    size(windowHeight,windowWidth)
    
def draw():
    background(255)
    fill(72,209,204)
    line(A[0], A[1], B[0], B[1])
    ellipse(A[0],A[1],5,5)
    ellipse(B[0],B[1],5,5)
    line(C[0], C[1], D[0], D[1])
    ellipse(C[0],C[1],5,5)
    ellipse(D[0],D[1],5,5)
    try:
        if intersects():
            P = getIntersectionPoint()
            fill(255,20,147)
            ellipse(P[0],P[1],8,8)
    except ZeroDivisionError:
        # There is a vertical line
        if A[0] == B[0]:
            #AB is a vertical line
            x = A[0]
            if x <= max (C[0], D[0]) and x >= min(C[0], D[0]):
                a = getAngularCoefficient(C,D)
                b = getLinearCoefficient(C,D)
                y = a*x + b
                if y <= max (A[1], B[1]) and y >= min (A[1], B[1]):
                    fill(255,20,147)
                    ellipse(x,y,8,8)
        else:
            #CD is a vertical line   
            x = C[0]
            if x <= max (A[0], B[0]) and x >= min(A[0], B[0]):
                a = getAngularCoefficient(A,B)
                b = getLinearCoefficient(A,B)
                y = a*x + b
                if y <= max (C[1], D[1]) and y >= min (C[1], D[1]):
                    fill(255,20,147)
                    ellipse(x,y,8,8)
        print "zero division error"

def mousePressed():
    #when user presses the mouse left button
    points = [A,B,C,D]
    for P in points:
        if dist(P[0], P[1], mouseX, mouseY) <= 5:
            S[0], S[1] = P[0], P[1]
            break
    
def mouseDragged():
    points = [A,B,C,D]
    for P in points:
        if S[0] == P[0] and S[1] == P[1] and mouseX < windowWidth and mouseX > 0 and mouseY < windowHeight and mouseY > 0:
            P[0], P[1] = mouseX, mouseY
            S[0], S[1] = mouseX, mouseY
            break
    
def mouseReleased():
    S[0], S[1] = -10, -10 
            
def parallels():
    # returns True if the line segments are parallel, False otherwise
    a0 = getAngularCoefficient(A,B)
    a1 = getAngularCoefficient(C,D)
    if a0 == a1:
        return True
    else:
        return False
    
def getAngularCoefficient(P, Q):
    # returns the angular coefficient of the line segment between points PQ
    x0 = P[0]
    y0 = P[1]
    x1 = Q[0]
    y1 = Q[1]
    if x1 != x0:
        a = float(y1 - y0)/(x1 - x0)
    else:
        a = tan(90)    
    return a

def getLinearCoefficient(P,Q):
    # returns the linear coefficient of the line segment between points PQ
    x0 = P[0]
    y0 = P[1]
    x1 = Q[0]
    y1 = Q[1]
    if (x0 != x1):
        b = float((x0*y1) - (x1*y0))/(x0-x1)
    else:
        # vertical line
        raise ZeroDivisionError    
    return b     
            
def getIntersectionPoint():
    # returns the point where the intersection between the 2 lines happen
    # since we already know the lines are not parallel, this relation is true
    # ax + b - y = cx + d - y = 0
    a = getAngularCoefficient(A,B)
    b = getLinearCoefficient(A,B)
    c = getAngularCoefficient(C,D)
    d = getLinearCoefficient(C,D)
    x = float(d - b)/(a - c)
    y = a*x + b
    P = [x,y]
    print P
    return P

def intersects():
    # returns True if the line segments intersects, False otherwise
    # be careful in case the segments are parallel
    if parallels():
        return False
    else:
        # AB intersects with CD ?
        P = getIntersectionPoint()
        x = P[0]
        y = P[1]
        # Checking if P is on both segments
        if x <= max(A[0], B[0]) and x >= min(A[0], B[0]) and x <= max(C[0], D[0]) and x >= min(C[0], D[0]):
            if y <= max(A[1], B[1]) and y >= min(A[1], B[1]) and y <= max (C[1], D[1]) and y >= min(C[1], D[1]):
                return True
        return False 
    
